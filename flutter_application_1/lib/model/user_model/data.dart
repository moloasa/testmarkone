class Data {
  int? id;
  String? username;
  String? password;
  int? score;
  String? telNumber;

  Data({this.id, this.username, this.password, this.score, this.telNumber});

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json['id'] as int?,
        username: json['username'] as String?,
        password: json['password'] as String?,
        score: json['score'] as int?,
        telNumber: json['tel_number'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'username': username,
        'password': password,
        'score': score,
        'tel_number': telNumber,
      };
}
