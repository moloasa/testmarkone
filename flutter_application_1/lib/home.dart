import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/model/user_model/user_model.dart';
import 'package:flutter_application_1/second.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Home extends StatelessWidget {
  Home({
    Key? key,
  }) : super(key: key);

  TextEditingController textController = TextEditingController();
  TextEditingController textController1 = TextEditingController();

  UserModel user = UserModel();

  Future login(String username, String password, BuildContext context) async {
    var dio = Dio();
    final response = await dio.get(
        'http://192.168.1.73:3000/users/user?username=$username&password=$password');

    if (response.data != 'not found') {
      user = UserModel.fromJson(response.data);

      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Second(user: user)),
      );
    } else {
      Fluttertoast.showToast(
          msg: "false ! Username $username  Password $password ",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.TOP,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }

    print(response.data);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            decoration: BoxDecoration(
              boxShadow: const [
                BoxShadow(
                  color: Colors.grey,
                  spreadRadius: 2,
                  blurRadius: 5,
                )
              ],
              borderRadius: BorderRadius.circular(15),
              border: Border.all(color: Colors.blue),
            ),
            padding: const EdgeInsets.all(20),
            child: const Text('Hello ',
                style: TextStyle(color: Colors.blue, fontSize: 40)),
          ),
          const SizedBox(
            height: 50,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: TextField(
              controller: textController,
              obscureText: false,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Username',
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: TextField(
              controller: textController1,
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Password',
              ),
            ),
          ),
          const SizedBox(
            height: 50,
          ),
          ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Colors.red),
              onPressed: () async {
                await login(textController.text, textController1.text, context);

                // String data = 'false';
                // if (textController.text == 'admin' &&
                //     textController1.text == 'admin') {
                //   data = 'true';
                // }

                // print(textController.text);
              },
              child: const Text('Page 1'))
        ],
      ),
    );
  }
}
